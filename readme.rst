####################
PROJETO COMPLEXIDADE
####################

Projeto desenvoldido afim de solucionar problema proposto em atividade na cadeira de complexidade
de software, o projeto visa montar arquivos de testes e testar o algoritmo desenvolvido de modo que o mesmo
possa ser analisado pela docente na referida disciplina.

******************
Problema proposto:
******************
Uma certa quantidade de tarefas J1, J2,...,Jn tem que ser processada em uma máquina. Após
cada tarefa, a máquina deve ser ajustada para atender aos requerimentos da próxima tarefa.
Dado o tempo tij de ajuste da tarefa Ji para a tarefa Jj, deseja-se encontrar a sequência de
tarefas que minimiza o tempo total de ajuste da máquina.


***********
Instalação
***********

Necessário PHP versão acima de 5.6, Apache, MySQL e GIT recomendado baixar o xampp que já traz tudo pronto 
`XAMPP <https://www.apachefriends.org/pt_br/index.html>`

Após baixar o XAMPP acessar o diretorio C:\xampp\htdocs\

Clonar projeto: git clone https://eric218110@bitbucket.org/eric218110/complexidade.git, com Git Bash, CMD ou Power Shell

Abrir o XAMPP Control Panel

Iniciar no  XAMPP o Apache e MySQL

Acessar <http://localhost/complexidade/> o projeto em algum navegador, recomendo o Google Chrome
Qualquer dúvida enviar email para ericmendes@si.fiponline.edu.br

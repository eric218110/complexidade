<?php
/**
* Index Page for this controller.
*
* Projeto complexidade
* 	Equipe:
*	ERIC SILVA MENDES
*   SAMUEL FERNANDES
*	
*/
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	} 

	public function index()
	{
		
		loadModals();

		$homePage = true;

		if ($homePage) {
			$this->load->view('home');
		} else {
		}
	}
}

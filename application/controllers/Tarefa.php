<?php
/**
* Index Page for this controller.
*
* Projeto complexidade
* 	Equipe:
*	ERIC SILVA MENDES
*   SAMUEL FERNANDES
*	
*/
defined('BASEPATH') OR exit('No direct script access allowed');

class Tarefa extends CI_Controller {

	public function index()
	{
        $this->load->view('tarefa');
    }
    
    public function cadastrar()
    {
        //HELPER PARA FORM
        $this->load->helper('form'); 
        //PEGAR MEUS POSTS
        $x = $this->input->post('x')+1;
        $y = $this->input->post('y');
        //CRIAR MEUS ARRAYS
        $valuesX = array('0' => 0);
        $valuesY = array('0' => "#");
        //ITERAR OS VALORES DE X
        for ($i=1; $i < $x; $i++) { 
            array_push($valuesX, strval($i));
        }
        //ITERAR OS VALORES DE Y
        for ($i=1; $i < $y; $i++) { 
           array_push($valuesY, strval($i));
        }
        //ADICIONAR MEUS DADOS AO JSON
        $jsonfile = array(
            'values' => array(
                'cabecalho' => $valuesX, 
                'x' => $valuesX, 
                'y' => $valuesY),
            'message' => "Valor cadastrado com sucesso !!!",
            'request' => true,
            'active'  => false,
        );
        //ABRIR MEUS JSON EM MODO DE LEITURA
        $fp = fopen('results.json', 'w');
        //ESCREVER OS VALORES EM MEU JSON
        fwrite($fp, json_encode($jsonfile, JSON_PRETTY_PRINT));
        //FECHAR MEU ARQUIVO :)
        fclose($fp);
        //REDIRECIONANDO MINHA PAGINA
        redirect("tarefa/evento");

    }

    function evento()
    {
        $lista = file_get_contents(import_host()."complexidade/results.json");

        $filesJson = json_decode($lista);

        $data['data'] = $filesJson;
        $this->load->view('evento', $data); 
    }
}
<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="pt-br">

<head>
    <?php import_assets(null, 'bootstrap-css'); ?>
    <?php import_assets('public/css/style.css', 'css') ?>
</head>

<body>
    <br><br><br>
    <div class="container">
        <div class="card">
            <div class="card-body">
                <div class="card">
                    <div class="card-header bg-primary">
                        <h6 class="text-center text-white">
                            Preencha os campos abaixo
                        </h6>
                    </div>
                    <div class="card-body">
                        <div class="row">
                            <form class="form-inline" method="post" action="tarefa/cadastrar">
                                <div class="cols-lg-5 col-md-5 col-sm-5 col-xs-5">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-lg-4 col-sm-4 col-xs-4 col-md-4">
                                                <label for="">Valor de X</label>
                                            </div>
                                            <div class="col-lg-8 col-sm-8 col-xs-8 col-md-8">
                                                <input type="text" name="x" id="x" class="form-control" placeholder="" aria-describedby="helpId" required>
                                                <small id="helpId" class="text-muted">Representa quantas horas a maquina irá trabalhar </small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="cols-lg-5 col-md-5 col-sm-5 col-xs-5">
                                    <div class="form-group">
                                        <div class="row">
                                            <div class="col-lg-4 col-sm-4 col-xs-4 col-md-4">
                                                <label for="">Valor de Y</label>
                                            </div>
                                            <div class="col-lg-8 col-sm-8 col-xs-8 col-md-8">
                                                <input type="text" name="y" id="y" class="form-control" placeholder="" aria-describedby="helpId" required>
                                                <small id="helpId" class="text-muted">Representa quantos processos haverá na maquina</small>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="cols-lg-2 col-md-2 col-sm-2 col-xs-2">
                                    <button type="submit" class="btn btn-primary">Cadastrar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
<?php import_assets(null, 'bootstrap-js'); ?>

</html>